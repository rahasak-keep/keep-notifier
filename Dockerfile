FROM golang:1.9

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install dependencies
RUN go get gopkg.in/mgo.v2
RUN	go get github.com/gorilla/mux
RUN go get github.com/Shopify/sarama
RUN go get github.com/wvanbergen/kafka/consumergroup
RUN go get github.com/gorilla/handlers
RUN go get -u github.com/sideshow/apns2

# copy app
ADD . /app
WORKDIR /app

# build
RUN go build -o build/notifier src/*.go

# server running port
EXPOSE 8762

# .keys volume
VOLUME ["/app/.keys"]

ENTRYPOINT ["/app/docker-entrypoint.sh"]
