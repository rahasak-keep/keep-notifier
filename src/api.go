package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Notification struct {
	DeviceType  string `json:"deviceType"`
	DeviceToken string `json:"deviceToken"`
	Title       string `json: "title"`
	Body        string `json: "body"`
	Message     string `json:"message"`
}

func initApi() {
	// router
	r := mux.NewRouter()
	r.HandleFunc("/api/notifications", apiNotification).Methods("POST")

	// start server
	err := http.ListenAndServe(":8762", r)
	if err != nil {
		log.Printf("ERROR: fail init http server, %s", err.Error)
		os.Exit(1)
	}
}

func apiNotification(w http.ResponseWriter, r *http.Request) {
	// read body
	data, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf("INFO: request notification %s", string(data))

	// unmarshal json
	var notification Notification
	err := json.Unmarshal(data, &notification)
	if err != nil {
		log.Printf("ERROR: fail to unmarshla json, %s", err.Error())
		response(w, "Invalid request json", 400)
		return
	}

	resp, status := notify(&notification)
	response(w, resp, status)
}

func response(w http.ResponseWriter, resp string, status int) {
	w.WriteHeader(status)
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(resp))
}
