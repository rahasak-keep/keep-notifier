package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func notify(notification *Notification) (string, int) {
	if notification.DeviceType == "android" {
		// android msg
		android := make(map[string]string)
		android["title"] = notification.Title
		android["body"] = notification.Body
		android["message"] = notification.Message

		// request map
		jsonMap := make(map[string]interface{})
		jsonMap["to"] = notification.DeviceToken
		jsonMap["data"] = android

		// marshel notification
		j, _ := json.Marshal(jsonMap)
		return post(j)
	} else {
		// apple msg
		apple := make(map[string]string)
		apple["title"] = notification.Title
		apple["body"] = notification.Body
		apple["message"] = notification.Message

		// request map
		jsonMap := make(map[string]interface{})
		jsonMap["to"] = notification.DeviceToken
		jsonMap["content_available"] = true
		jsonMap["notification"] = apple

		// marshel notification
		j, _ := json.Marshal(jsonMap)
		return post(j)
	}
}

func post(data []byte) (string, int) {
	log.Printf("INFO: send request to, %s with %s", fcmConfig.api, string(data))

	// request
	req, err := http.NewRequest("POST", fcmConfig.api, bytes.NewBuffer(data))
	if err != nil {
		log.Printf("ERROR: fail init fcm android request, %s", err.Error)
		return "error request", 400
	}

	// headers
	key := "key=" + fcmConfig.serverKey
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", key)

	// send request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Printf("ERROR: fail send fcm request, %s", err.Error())
		return "error request", 400
	}
	defer resp.Body.Close()
	b, _ := ioutil.ReadAll(resp.Body)

	// TODO parse response and check success=1

	log.Printf("INFO: got response %s, status %d", string(b), resp.StatusCode)

	return string(b), resp.StatusCode
}
